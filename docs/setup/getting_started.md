<a name="downloadthetheme"></a>
### 1#Download the theme
Ready to get started? First, you need to download your theme.

1. Access your account at <a href="http://bindtuning.com" target="_blank">bindtuning.com</a>;
2. Go to **My Downloads**, mouse hover the theme and click on **View Theme**, to open the theme details page;
3. Last but not least, click on **Download**.

![kentico9_rtd_gettingstarted.png](https://bitbucket.org/repo/yG87gj/images/2201563596-kentico9_rtd_gettingstarted.png)


Ok, on to unzipping the file.

----

<a name="unzipthefile"></a>
### 2#Unzip the file ###

After unzipping your theme package, you will find four folders, a images folder, a CSS folder, a fonts folder and a js folder. Inside your theme's package, you will also find a number of .ascx files and .css files that you will be using to install your theme.

 ── **CSS** 📁 

 ── **fonts** 📁 
 
 ── **images** 📁

 ── **js** 📁 

-----


Now let's get your website rocking! First thing on the list: [Installation](http://bindtuning-kentico-8-9-10-themes.readthedocs.io/en/latest/SETUP/Installation/). 


